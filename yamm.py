import os
from pathlib import Path
import providers
import requests
import shutil
from classes.Database import Database
from pyunpack import Archive


class YAMM:

    mod_directory = "mods"

    def __init__(self, db_path):
        db = Database(db_path)
        self.db = db
        self.current_profile = None

    def set_profile(self, profile):
        self.current_profile = profile

    """Turns an URL into a mod object."""
    def import_url(url):
        pro = providers.find_provider(url)
        if pro:
            mod = pro.get_mod_obj(url)
            return mod
        else:
            return False

        return mod

    """Download file to mod directory without showing progress."""
    def simple_download(self, url, name, mod):
        download_path = "{YAMM.mod_directory}/{mod.id}"
        if not os.path.exists(YAMM.mod_directory):
            print("Specified mod directory does not exist.")
            exit()
        if not os.path.exists(download_path):
            os.mkdir(download_path)
        r = requests.get(url, allow_redirects=True, stream=True)
        download_iter = r.iter_content(chunk_size=1024)
        with open(f'{download_path}/{name}', 'wb') as file:
            for chunk in download_iter:
                if chunk:
                    file.write(chunk)
        self.post_download(mod, f'{download_path}/{name}')

    """Download file to mod directory with support for progress report."""
    def progress_download(self, url, name, mod, progress):
        download_path = f"{YAMM.mod_directory}/{mod.id}"
        if not os.path.exists(YAMM.mod_directory):
            print("Specified mod directory does not exist.")
            exit()
        if not os.path.exists(download_path):
            os.mkdir(download_path)
        r = requests.get(url, allow_redirects=True, stream=True)
        with open(f'{download_path}/{name}', 'wb') as file:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    file.write(chunk)
                    progress.update(1024)
        self.post_download(mod, f'{download_path}/{name}')

    def install(mod):
        print("Yeehaw")

    """Tries to convert the downloaded file into a YAMM mod folder."""
    def post_download(self, mod, dl_file):
        game = self.current_profile.game
        dl_file = Path(dl_file)
        mod_dir = dl_file.parent

        # Extract it if needed
        if dl_file.suffix in game.archive_types:
            Archive(dl_file).extractall(mod_dir)
            dl_file.unlink()

        # All good if supported type
        if any(item.suffix in game.file_types for item in mod_dir.iterdir()):
            return True

        # Else we try to make the folder a game arch folder
        extracted_files = []
        for ext_file in mod_dir.iterdir():
            extracted_files.append(ext_file)
        game_arch_path = self.current_profile.game.find_game_arch(mod_dir)
        for mod_file in game_arch_path.iterdir():
            shutil.move(mod_file, mod_dir)
        for old_file in extracted_files:
            if old_file.is_dir():
                old_file.rmdir()
            else:
                old_file.unlink()

        return True
