import providers


class Mod:

    def __init__(self, id, name, provider_id, external_id, external_link):
        self.id = id
        self.name = name
        self.provider = providers.listing[provider_id]
        self.external_id = external_id
        self.external_link = external_link
        self.files = []

    def get_dl_links(self):
        links = self.provider.get_mod_dl_links(self)
        return links
