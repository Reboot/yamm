import requests


class Provider:

    cache = {}

    def url_matches(self, url):
        if self.base_url in url:
            return True
        else:
            return False

    def cache_request(url):
        if url not in Provider.cache:
            req = requests.get(url)
            Provider.cache[url] = req
        return Provider.cache[url]
