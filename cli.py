from yamm import YAMM as YammClass
import sys
import click

yamm = YammClass("yamm.db")


@click.group("yamm_cli")
@click.pass_context
def yamm_cli(ctx):
    """An example CLI for interfacing with a document"""
    # _stream = open(document)
    # _dict = json.load(_stream)
    # _stream.close()
    # ctx.obj = _dict


@yamm_cli.command("download")
@click.argument("url")
@click.pass_context
def download(ctx, url):
    profiles = yamm.db.profile_get_all()
    if len(profiles) == 1:
        print("Only one existing profile, using it")
        yamm.set_profile(profiles[0])
    else:
        print("Multiple existing profiles, please pick one")
    mod = YammClass.import_url(url)
    if not mod:
        print(click.style("No providers matched.", fg="red"))
        sys.exit(0)
    print(f"Downloading {mod.name}")
    links = mod.get_dl_links()
    if len(links) > 1:
        choice_number = 1
        formatted_links = "Which file would you like to download?\n"
        for index, dict in links.items():
            formatted_links += f"{choice_number}. {dict['name']}\n"
            choice_number += 1
        chosen_link = click.prompt(
            formatted_links,
            type=click.Choice([str(i) for i in range(1, choice_number)]),
            default='1'
        )
    else:
        chosen_link = 1
    file = links[int(chosen_link) - 1]
    with click.progressbar(length=file["size"], label=f"Downloading {file['name']}") as bar:
        yamm.progress_download(file['link'], file['name'], mod, bar)
    print("Finsihed!")


@yamm_cli.group("profile")
@click.pass_context
def profile(ctx):
    pass


@profile.command("list")
@click.pass_context
def profile_list(ctx):
    profiles = yamm.db.profile_get_all()
    for profile in profiles:
        print(f"{profile.id}: {profile.game.name} - {profile.path}")


def main():
    yamm_cli(prog_name="yamm")


if __name__ == '__main__':
    main()
