from classes.Provider import Provider
from classes.Mod import Mod
from classes.Tools import Tools
import re
import requests


class GameBanana(Provider):

    id = "gamebanana"
    name = "Gamebanana"
    base_url = "gamebanana.com"
    api_url = "https://gamebanana.com/apiv4/Mod/"
    urls = ""

    @staticmethod
    def get_mod_obj(url):
        mod_id = re.search("([0-9]+)$", url).group()
        api_request = f"{GameBanana.api_url}{mod_id}"
        resp = Provider.cache_request(api_request)
        resp_dict = resp.json()
        id = Tools.flatten_string(resp_dict.get('_sName'))
        mod = Mod(id, resp_dict.get('_sName'), GameBanana.id, mod_id, url)
        return mod

    @staticmethod
    def get_mod_dl_links(mod):
        dl_links = dict()
        api_request = f"{GameBanana.api_url}{mod.external_id}"
        resp = requests.get(api_request)
        resp_dict = resp.json()
        file_index = 0
        for file in resp_dict.get("_aFiles"):
            r_file = {}
            r_file['name'] = file['_sFile']
            r_file['size'] = file['_nFilesize']
            r_file['link'] = file['_sDownloadUrl']
            dl_links[file_index] = r_file
            file_index += 1
        return dl_links


GB = GameBanana()
