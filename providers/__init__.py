from providers.GameBanana import GB

# TODO: Import providers dynamically

listing = {}
listing[GB.id] = GB


def find_provider(url):
    for id, p in listing.items():
        if p.url_matches(url):
            return p
    return False
